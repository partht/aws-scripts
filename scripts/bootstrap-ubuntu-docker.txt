#!/bin/bash

sudo su - ubuntu

sudo apt update -y && sudo apt upgrade -y 

sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update -y

sudo apt-get install -y docker-ce docker-ce-cli containerd.io

sudo groupadd docker

sudo usermod -aG docker $USER

sudo usermod -aG docker ubuntu

newgrp docker

sudo systemctl enable docker

# Following lines for NGINX Web Server
sudo apt install -y nginx
sudo systemctl start nginx
sudo systemctl enable nginx

sudo mkdir -p /apps/data/dbdata/pgsql 
sudo mkdir -p /apps/data/dbdata/mongodb

sudo chmod -R 777 /apps 

sudo apt install net-tools -y

# Installing Docker Compose

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Installing AWS CLI
sudo apt install unzip -y
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install -i /usr/local/aws-cli -b /usr/local/bin


# Restarting the instance
sudo shutdown -r